import { Module } from '@nestjs/common';
import { TempService } from './temperature.service ';
import { TemperatureController } from './temperature.controller';

@Module({
  imports: [],
  controllers: [TemperatureController],
  providers: [TempService],
  exports: [],
})
export class TempModule {}
